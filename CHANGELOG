# Changelog

Tous changement notable à ce projet sera documenté dans ce fichier.

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
et ce projet adhère à [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.1] - 2024-04-30

### Correctif

- Corrige un bug lors de la conversion d'une réponse XML

## [1.2.0] - 2024-04-30

### Ajout

- Un Preferences DataStore pour persister le nom de l'utilisateur actif (#6)
- La carte d'un utilisateur peut être appuyée pour le désigner comme utilisateur actif
- Collecte la collection de jeux à partir de l'API de BoardGameGeek lors de la création d'un utilisateur (#3)
- Un bouton sur la carte d'un utilisateur pour mettre à jour sa collection de jeux
- Sauvegarde la collection de jeux d'un utilisateur dans la base de donnée (#4)
- Barre d'action dans l'écran Jeux
- Menu déroulant pour choisir d'afficher la liste des jeux populaires ou la collection de l'utilisateur actif
- Affiche la collection de jeux de l'utilisateur actif (#5)
- SplashScreen lors de l'ouverture de l'application
- Icône de l'application

### Correctif

- Duplication de jeux populaires lors du rafraîchissement (#1)
- Correction de plusieurs bugs relié aux appels d'API
- Les images dans les cartes de jeux sont redimensionnés pour les rendres uniformes

### Changement

- Le rang d'un jeu populaire est utilisé comme clé naturelle dans la table JeuPopulaire (#2)
- La fonctionnalité de rafraîchissement des jeux populaires à été déplacé dans la barre d'action de l'écran de jeux

### Retrait

- Floating Action Button dans l'écran de liste des jeux

## [1.1.0] - 2024-04-11

### Ajout

- Liste des jeux populaires sur BoardGameGeek
- Écran des utilisateurs
- Création d'un utilisateur
- suppression d'un utilisateur
- Barre de navigation vers les 2 Sections principales

### Changement

- Mises à jour dépendances: Java 21, Gradle 8.7
