# LudoToolz

## Description
LudoToolz est une Application Android qui offre aux usagers plusieurs outils pour accompagner une soirée de jeux de société.

## Pré-requis
- Android Studio (version Iguana | 2023.2.1 ou supérieur)
- Une image d'émulateur ou un appareil Android avec Android 14 (api 34)
- JDK Java 21

## Installation

1. Cloner le projet sur votre ordinateur.
2. Dans Android Studio, ouvrir le projet. Connecter votre appareil Android ou configurer l'émulateur si ce n'est pas déjà fait.
3. Dans Android Studio, sélectionner votre appareil et démarrer l'application.

## Feuille de route

- Filtrer la liste des jeux selon plusieurs critères, comme la catégorie, le nombre de joueurs etc...
- Lier les expansions aux jeux.
- Écran pour les outils. 

## License
Copyright 2024 Alec Maltais

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
