package com.example.ludotoolz.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "jeux")
data class Jeu(
    @PrimaryKey val jeuId: Int = 0,
    @ColumnInfo(name = "titre") val titre: String,
    @ColumnInfo(name = "thumbnail") val thumbnail: String,
    @ColumnInfo(name = "image") val image: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "joueurs_min") val joueursMin: Int,
    @ColumnInfo(name = "joueurs_max") val joueursMax: Int,
    @ColumnInfo(name = "temps_moyen") val tempsMoyen: Int
)
