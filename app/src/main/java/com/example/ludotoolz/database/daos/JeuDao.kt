package com.example.ludotoolz.database.daos

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.ludotoolz.database.models.Jeu
import kotlinx.coroutines.flow.Flow

@Dao
interface JeuDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertALL(vararg jeux: Jeu)

    @Delete
    fun delete(jeu: Jeu)

    @Query("SELECT * FROM jeux ORDER BY titre ASC")
    fun getALL(): Flow<List<Jeu>>
}