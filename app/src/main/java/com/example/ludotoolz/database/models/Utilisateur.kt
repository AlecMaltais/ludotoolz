package com.example.ludotoolz.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "utilisateurs")
data class Utilisateur(
    @PrimaryKey(autoGenerate = true) val utilisateurId: Int = 0,
    @ColumnInfo(name = "nom") val nomUtilisateur: String,
    @ColumnInfo(name = "est_selectionne") var estSelectionne: Boolean,
)
