package com.example.ludotoolz.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.ludotoolz.database.daos.JeuDao
import com.example.ludotoolz.database.daos.JeuPopulaireDao
import com.example.ludotoolz.database.daos.UtilisateurDao
import com.example.ludotoolz.database.daos.UtilisateurJeuDao
import com.example.ludotoolz.database.models.Jeu
import com.example.ludotoolz.database.models.JeuPopulaire
import com.example.ludotoolz.database.models.Utilisateur
import com.example.ludotoolz.database.models.UtilisateurJeuCrossRef

@Database(version = 9, entities = [Jeu::class, Utilisateur::class, JeuPopulaire::class, UtilisateurJeuCrossRef::class], exportSchema = false)
abstract class LudoDatabase : RoomDatabase() {

    abstract fun getJeuDao(): JeuDao

    abstract fun getUtilisateurDao(): UtilisateurDao

    abstract fun getJeuPopulaireDao(): JeuPopulaireDao

    abstract fun getUtilisateurJeuDao(): UtilisateurJeuDao
}