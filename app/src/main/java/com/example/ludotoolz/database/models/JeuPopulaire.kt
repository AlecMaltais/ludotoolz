package com.example.ludotoolz.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "jeux_populaires")
data class JeuPopulaire(
    @PrimaryKey val rang: Int,
    @ColumnInfo(name = "nom") val nom: String,
    @ColumnInfo(name = "thumbnail") val thumbnail: String,
    @ColumnInfo(name = "annee_publication") val anneePublication: Int
)
