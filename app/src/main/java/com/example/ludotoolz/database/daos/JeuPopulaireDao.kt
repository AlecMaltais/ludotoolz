package com.example.ludotoolz.database.daos

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.ludotoolz.database.models.JeuPopulaire
import kotlinx.coroutines.flow.Flow

@Dao
interface JeuPopulaireDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertALL(vararg jeuxPopulaires: JeuPopulaire)

    @Delete
    fun delete(vararg jeuxPopulaires: JeuPopulaire)

    @Query("DELETE FROM jeux_populaires")
    fun deleteALL()

    @Query("SELECT * FROM jeux_populaires ORDER BY rang ASC")
    fun getALL(): Flow<List<JeuPopulaire>>
}