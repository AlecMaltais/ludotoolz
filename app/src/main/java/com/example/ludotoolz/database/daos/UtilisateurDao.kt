package com.example.ludotoolz.database.daos

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.example.ludotoolz.database.models.Utilisateur
import kotlinx.coroutines.flow.Flow

@Dao
interface UtilisateurDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUtilisateurs(vararg utilisateurs: Utilisateur)

    @Update
    suspend fun updateUtilisateurs(vararg utilisateurs: Utilisateur)

    @Delete
    suspend fun deleteUtilisateur(vararg utilisateur: Utilisateur)

    @Query("SELECT * FROM utilisateurs ORDER BY nom ASC")
    fun loadUtilisateurs(): Flow<List<Utilisateur>>

    @Query("SELECT * FROM utilisateurs WHERE utilisateurId = :id")
    fun loadUtilisateurById(id: Int): Flow<Utilisateur>

    @Query("SELECT * FROM utilisateurs WHERE nom = :username")
    fun loadUtilisateurByUserName(username: String): Flow<Utilisateur>

    @Query("SELECT utilisateurId FROM utilisateurs WHERE nom = :username")
    fun getIdByUserName(username: String): Int

    @Query("SELECT EXISTS(SELECT * FROM utilisateurs WHERE nom = :username)")
    fun verifieUtilisateurExiste(username : String) : Boolean
}