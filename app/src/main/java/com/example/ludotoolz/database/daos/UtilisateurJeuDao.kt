package com.example.ludotoolz.database.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.example.ludotoolz.database.models.UtilisateurAvecJeux
import com.example.ludotoolz.database.models.UtilisateurJeuCrossRef
import kotlinx.coroutines.flow.Flow

@Dao
interface UtilisateurJeuDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(join: UtilisateurJeuCrossRef)

    @Transaction
    @Query("SELECT * FROM utilisateurs WHERE utilisateurId = :id")
    fun loadUtilisateurAvecJeuxById(id: Int): Flow<UtilisateurAvecJeux>

    @Query("DELETE FROM UtilisateurJeuCrossRef WHERE utilisateur = :id")
    fun deleteAllUtilisateurJeuCrossRefByUtilisateurId(id: Int)
}