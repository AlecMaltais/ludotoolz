package com.example.ludotoolz.database.models

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Junction
import androidx.room.Relation

@Entity(primaryKeys = ["utilisateur", "jeu"], foreignKeys = [
    ForeignKey(
        entity = Utilisateur::class,
        parentColumns = ["utilisateurId"],
        childColumns = ["utilisateur"],
        onDelete = ForeignKey.CASCADE
    ),
    ForeignKey(
        entity = Jeu::class,
        parentColumns = ["jeuId"],
        childColumns = ["jeu"],
        onDelete = ForeignKey.CASCADE
    )
])
data class UtilisateurJeuCrossRef(
    val utilisateur: Int,
    val jeu: Int
)

data class UtilisateurAvecJeux(
    @Embedded
    val utilisateur: Utilisateur = Utilisateur(0, "", true),
    @Relation(
        parentColumn = "utilisateurId",
        entityColumn = "jeuId",
        associateBy = Junction(
            value = UtilisateurJeuCrossRef::class,
            parentColumn = "utilisateur",
            entityColumn = "jeu")
    )
    val jeux: List<Jeu> = listOf()
)
