package com.example.ludotoolz.Datastore

import kotlinx.coroutines.flow.Flow

interface UserPreference {

    fun userName(): Flow<String>

    suspend fun saveUserName(name: String)
}