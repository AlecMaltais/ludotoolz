package com.example.ludotoolz.module

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.room.Room
import com.example.ludotoolz.Datastore.UserDataStore
import com.example.ludotoolz.MainActivityViewModel
import com.example.ludotoolz.data.ConstantesAPI
import com.example.ludotoolz.data.repositories.JeuPopulaireRepository
import com.example.ludotoolz.data.repositories.JeuRepository
import com.example.ludotoolz.database.LudoDatabase
import com.example.ludotoolz.network.JeuPopulaireService
import com.example.ludotoolz.data.repositories.OfflineUtilisateursRepository
import com.example.ludotoolz.data.repositories.OfflineJeuPopulaireRepository
import com.example.ludotoolz.data.repositories.OfflineUtilisateurAvecJeuxRepository
import com.example.ludotoolz.data.repositories.OfflineJeuRepository
import com.example.ludotoolz.data.repositories.UtilisateurAvecJeuxRepository
import com.example.ludotoolz.data.repositories.UtilisateursRepository
import com.example.ludotoolz.network.JeuService
import com.example.ludotoolz.network.UtilisateursService
import com.example.ludotoolz.ui.viewmodels.EntreUtilisateurViewModel
import com.example.ludotoolz.ui.viewmodels.JeuxEcranViewModel
import com.example.ludotoolz.ui.viewmodels.ListeUtilisateursViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.dsl.bind
import org.koin.core.module.dsl.singleOf
import org.koin.core.qualifier.named
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory

private fun getRetrofitInstance(
    url: String
): Retrofit {
    return Retrofit.Builder()
        .baseUrl(url)
        .addConverterFactory(SimpleXmlConverterFactory.create())
        .build()
}

private val Context.datastore : DataStore<Preferences> by preferencesDataStore(name = "ludotoolz.shared.preferences")

/**
 * Injection de dépendance.
 */
val appModule = module {

    single { UserDataStore(androidContext().datastore) }
    single {
        Room.databaseBuilder(
            androidContext(),
            LudoDatabase::class.java,
            "ludo-database"
        ).fallbackToDestructiveMigration()
            .build()
    }
    single { get<LudoDatabase>().getUtilisateurDao() }
    single { get<LudoDatabase>().getJeuDao() }
    single { get<LudoDatabase>().getJeuPopulaireDao() }
    single { get<LudoDatabase>().getUtilisateurJeuDao() }
    factory(named(ConstantesAPI.BGGAPI2.NOM)) {
        getRetrofitInstance(ConstantesAPI.BGGAPI2.URL)
    }
    factory {
        get<Retrofit>(named(ConstantesAPI.BGGAPI2.NOM))
            .create(JeuPopulaireService::class.java)
    }
    factory {
        get<Retrofit>(named(ConstantesAPI.BGGAPI2.NOM))
            .create(UtilisateursService::class.java)
    }
    factory {
        get<Retrofit>(named(ConstantesAPI.BGGAPI2.NOM))
            .create(JeuService::class.java)
    }

    singleOf(::OfflineUtilisateursRepository) { bind<UtilisateursRepository>() }
    singleOf(::OfflineJeuPopulaireRepository) { bind<JeuPopulaireRepository>() }
    singleOf(::OfflineUtilisateurAvecJeuxRepository) { bind<UtilisateurAvecJeuxRepository>() }
    singleOf(::OfflineJeuRepository) { bind<JeuRepository>() }

    viewModelOf(::MainActivityViewModel)
    viewModelOf(::JeuxEcranViewModel)
    viewModelOf(::EntreUtilisateurViewModel)
    viewModelOf(::ListeUtilisateursViewModel)
}