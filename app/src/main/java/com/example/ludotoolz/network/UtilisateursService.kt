package com.example.ludotoolz.network

import com.example.ludotoolz.data.model_xml.UtilisateurRemote
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface UtilisateursService {
    @GET("user")
    suspend fun get(
        @Query("name") nom: String
    ): Response<UtilisateurRemote>
}