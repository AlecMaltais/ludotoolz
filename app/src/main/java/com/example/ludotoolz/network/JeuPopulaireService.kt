package com.example.ludotoolz.network

import com.example.ludotoolz.data.model_xml.ItemsPopulaires
import retrofit2.Response
import retrofit2.http.GET

interface JeuPopulaireService {
    @GET("hot?type=boardgame")
    suspend fun get(
    ): Response<ItemsPopulaires>
}