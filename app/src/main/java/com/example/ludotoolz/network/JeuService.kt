package com.example.ludotoolz.network

import com.example.ludotoolz.data.model_xml.Items
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface JeuService {


    @GET("collection")
    suspend fun getJeux(
        @Query("username") nomUtilisateur: String,
        @Query("excludesubtype") subtype: String = "boardgameexpansion"
    ): Response<Items>

    @GET("collection")
    suspend fun getExpansions(
        @Query("username") nomUtilisateur: String,
        @Query("subtype") subtype: String = "boardgameexpansion"
    ): Response<Items>
}