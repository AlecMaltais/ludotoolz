package com.example.ludotoolz

import com.example.ludotoolz.module.appModule
import android.app.Application
//import com.example.ludotoolz.data.AppContainer
//import com.example.ludotoolz.data.AppDataContainer
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class LudoApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@LudoApplication)
            modules(appModule)
        }
    }
}