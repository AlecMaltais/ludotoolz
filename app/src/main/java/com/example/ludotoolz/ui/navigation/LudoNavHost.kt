package com.example.ludotoolz.ui.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost

@Composable
fun LudoNavHost(
    navController: NavHostController,
    startDestination: String = JEUX_ROUTE,
    modifier: Modifier = Modifier
) {
    NavHost(
        navController = navController,
        startDestination = startDestination,
        modifier = modifier,
    ) {
        jeuxEcran()
        utilisateurGraph(navController)
    }
}