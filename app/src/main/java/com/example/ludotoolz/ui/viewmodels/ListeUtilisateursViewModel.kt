package com.example.ludotoolz.ui.viewmodels


import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ludotoolz.Datastore.UserDataStore
import com.example.ludotoolz.data.repositories.JeuRepository
import com.example.ludotoolz.data.repositories.UtilisateurAvecJeuxRepository
import com.example.ludotoolz.database.models.Utilisateur
import com.example.ludotoolz.data.repositories.UtilisateursRepository
import com.example.ludotoolz.database.models.Jeu
import com.example.ludotoolz.database.models.UtilisateurJeuCrossRef
import com.example.ludotoolz.network.JeuService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class ListeUtilisateursViewModel(
    private val utilisateursRepository: UtilisateursRepository,
    private val jeuService: JeuService,
    private val jeuRepository: JeuRepository,
    private val utilisateurAvecJeuxRepository: UtilisateurAvecJeuxRepository,
    private val userDataStore: UserDataStore
): ViewModel() {

    private val _nomUtilisateurSelectionner = MutableStateFlow<String?>(null)

    val nomUtilisateurSelectionner: StateFlow<String?> = _nomUtilisateurSelectionner

    val utilisateursUiState: StateFlow<UtilisateursUiState> =
        utilisateursRepository.getAllUtilisateursStream().map { UtilisateursUiState(it) }
            .stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(TIMEOUT_MILLIS),
                initialValue = UtilisateursUiState()
            )

    /**
     * Supprime l'utilisteur dans la base de données. Vide la valeur du PreferencesDataStore
     * si elle correspond au nom de l'utilisateur à supprimer.
     *
     * @param utilisateur Un utilisateur.
     */
    fun supprimerUtilisateur(utilisateur: Utilisateur) {
        viewModelScope.launch {
            if(utilisateur.nomUtilisateur == userDataStore.userName().first())
                userDataStore.saveUserName("")

            utilisateursRepository.deleteUtilisateur(utilisateur)
        }
    }

    /**
     * Lance les méthodes pour obtenir et enregistrer la collection
     * d'un utilisateur de BoardGameGeek.
     */
    fun updateCollectionRemote(utilisateur: Utilisateur) {
        viewModelScope.launch(Dispatchers.IO) {
            updateJeuxRemote(utilisateur)
            updateExpansionsRemote(utilisateur)
        }
    }

    /**
     * Fait une requête à l'API de BoardGameGeek pour obtenir tous les jeux de spciétés
     * dans la collection d'un utilisateur et les enregistrer dans la base de données.
     *
     * @param utilisateur Un utilisateur
     */
    suspend fun updateJeuxRemote(utilisateur: Utilisateur) {
        var reponse = jeuService.getJeux(utilisateur.nomUtilisateur)

        while (reponse.isSuccessful && reponse.code() == 202) {
            delay(5000)
            reponse = jeuService.getJeux(utilisateur.nomUtilisateur)
        }

        if (reponse.isSuccessful && reponse.body()?.totale!! > 0) {
            val items = reponse.body()
            if (items != null) {
                val jeuxMap: List<Jeu> = items.listeItem.map {
                    Jeu(
                        jeuId = it.objectId,
                        titre = it.nom,
                        thumbnail = it.thumbnail,
                        image = it.image,
                        description = it.subtype,
                        joueursMin = it.statistique.minJoueurs,
                        joueursMax = it.statistique.maxJoueurs,
                        tempsMoyen = it.statistique.tempsJeu
                    )
                }
                jeuRepository.insertAllJeux(*jeuxMap.toTypedArray())

                for (jeu in jeuxMap) {
                    utilisateurAvecJeuxRepository.insertUtilisateurjeuCrossRef(
                        utilisateurJeuCrossRef = UtilisateurJeuCrossRef(
                            utilisateur.utilisateurId,
                            jeu.jeuId
                        )
                    )
                }
            }
        }
    }

    /**
     * Fait une requête à l'API de BoardGameGeek pour obtenir toutes les expansions
     * dans la collection d'un utilisateur et les enregistrer dans la base de données.
     *
     * @param utilisateur Un utilisateur
     */
    suspend fun updateExpansionsRemote(utilisateur: Utilisateur) {
        var reponse = jeuService.getExpansions(utilisateur.nomUtilisateur)

        while (reponse.isSuccessful && reponse.code() == 202) {
            delay(5000)
            reponse = jeuService.getExpansions(utilisateur.nomUtilisateur)
        }

        if (reponse.isSuccessful && reponse.body()?.totale!! > 0) {
            val items = reponse.body()
            if (items != null) {
                val jeuxMap: List<Jeu> = items.listeItem.map {
                    Jeu(
                        jeuId = it.objectId,
                        titre = it.nom,
                        thumbnail = it.thumbnail,
                        image = it.image,
                        description = it.subtype,
                        joueursMin = it.statistique.minJoueurs,
                        joueursMax = it.statistique.maxJoueurs,
                        tempsMoyen = it.statistique.tempsJeu
                    )
                }
                jeuRepository.insertAllJeux(*jeuxMap.toTypedArray())

                for (jeu in jeuxMap) {
                    utilisateurAvecJeuxRepository.insertUtilisateurjeuCrossRef(
                        utilisateurJeuCrossRef = UtilisateurJeuCrossRef(
                            utilisateur.utilisateurId,
                            jeu.jeuId
                        )
                    )
                }
            }
        }
    }

    /**
     * Sauvegarde l'utilisateur sélectionné dans les préférences.
     *
     * @param utilisateur l'utilisateur sélectionné
     */
    fun selectionnerUtilisateur(utilisateur: Utilisateur) {
        viewModelScope.launch {
            userDataStore.saveUserName(utilisateur.nomUtilisateur)
        }
    }

    init {
        viewModelScope.launch {
            userDataStore.userName().collect { userName ->
                _nomUtilisateurSelectionner.value = userName
            }
        }
    }

    companion object {
        private const val TIMEOUT_MILLIS = 5_000L
    }
}

data class UtilisateursUiState(val utilisateurList: List<Utilisateur> = listOf())