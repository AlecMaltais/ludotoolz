package com.example.ludotoolz.ui

import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavDestination
import androidx.navigation.NavDestination.Companion.hierarchy
import com.example.ludotoolz.R
import com.example.ludotoolz.ui.composante.LudoArrierePlan
import com.example.ludotoolz.ui.composante.LudoNavigationBar
import com.example.ludotoolz.ui.composante.LudoNavigationBarItem
import com.example.ludotoolz.ui.composante.LudoToolzTopAppBar
import com.example.ludotoolz.ui.navigation.LudoNavHost
import com.example.ludotoolz.ui.navigation.TopLevelDestination

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LudoApp(
    appState: LudoAppState = rememberLudoAppState()
) {
    LudoArrierePlan {
        Scaffold(
            modifier = Modifier,
            containerColor = Color.Transparent,
            contentColor = MaterialTheme.colorScheme.onBackground,
            contentWindowInsets = WindowInsets(0,0,0,0),
            topBar = {
                val destination = appState.topLevelDestinationActuelle
                if (destination != null) {
                    LudoToolzTopAppBar(
                        titreRes = destination.titreTexteId,
                        peuNaviguerArriere = false,
                        colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                            containerColor = Color.Transparent
                        ),
                        actionIcon = Icons.Filled.Settings,
                        actionIconContentDescription = stringResource(
                            id = R.string.settings_description
                        ),
                        onActionClick = { }
                    )
                }
            },
            bottomBar = {
                LudoBottomBar(
                    destinations = appState.topLevelDestinations,
                    onNavigateToDestination = appState::navigateToTopLevelDestination,
                    destinationActuelle = appState.destinationActuelle,
                )
            }
        ) { innerPadding ->
            LudoNavHost(
                navController = appState.navController,
                modifier = Modifier.padding(innerPadding)
            )
        }
    }
}

@Composable
private fun LudoBottomBar(
    destinations: List<TopLevelDestination>,
    onNavigateToDestination: (TopLevelDestination) -> Unit,
    destinationActuelle: NavDestination?,
    modifier: Modifier = Modifier
) {
    LudoNavigationBar(
        modifier = modifier
    ) {
        destinations.forEach { destination ->
            val selected = destinationActuelle.isTopLevelDestinationInHierarchy(destination)
            LudoNavigationBarItem(
                selected = selected,
                onClick = { onNavigateToDestination(destination) },
                icon = {
                    Icon(imageVector = destination.unselectedIcon,
                        contentDescription = null
                    )
                },
                selectedIcon = {
                    Icon(imageVector = destination.selectedIcon,
                        contentDescription = null
                    )
                },
                label = { Text(stringResource(destination.iconTexteId)) },
                modifier = Modifier
            )
        }
    }
}

private fun NavDestination?.isTopLevelDestinationInHierarchy(destination: TopLevelDestination) =
    this?.hierarchy?.any {
        it.route?.contains(destination.name, true) ?: false
    } ?: false