package com.example.ludotoolz.ui.navigation

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.material.icons.outlined.Person
import androidx.compose.material.icons.outlined.ShoppingCart
import androidx.compose.ui.graphics.vector.ImageVector
import com.example.ludotoolz.R

enum class TopLevelDestination(
    val selectedIcon: ImageVector,
    val unselectedIcon: ImageVector,
    val iconTexteId: Int,
    val titreTexteId: Int,
) {
    /*
    OUTILS
     */
    JEUX(
        selectedIcon = Icons.Filled.ShoppingCart,
        unselectedIcon = Icons.Outlined.ShoppingCart,
        iconTexteId = R.string.liste_jeux,
        titreTexteId = R.string.app_name
    ),
    UTILISATEURS(
        selectedIcon = Icons.Filled.Person,
        unselectedIcon = Icons.Outlined.Person,
        iconTexteId = R.string.utilisateur_titre,
        titreTexteId = R.string.utilisateur_titre
    )
}