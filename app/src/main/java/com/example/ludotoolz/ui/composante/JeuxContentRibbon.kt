package com.example.ludotoolz.ui.composante

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.material.icons.outlined.PlayArrow
import androidx.compose.ui.graphics.vector.ImageVector
import com.example.ludotoolz.R

enum class JeuxContentRibbon(
    val selectedIcon: ImageVector,
    val unselectedIcon: ImageVector,
    val iconTexteId: Int,
    val titreTexteId: Int,
) {
    COLLECTION_JEUX(
        selectedIcon = Icons.Filled.PlayArrow,
        unselectedIcon = Icons.Outlined.PlayArrow,
        iconTexteId = R.string.collection,
        titreTexteId = R.string.collection
    ),
    JEUX_POPULAIRES(
        selectedIcon = Icons.Filled.Favorite,
        unselectedIcon = Icons.Outlined.FavoriteBorder,
        iconTexteId = R.string.popular_games,
        titreTexteId = R.string.popular_games
    )
}