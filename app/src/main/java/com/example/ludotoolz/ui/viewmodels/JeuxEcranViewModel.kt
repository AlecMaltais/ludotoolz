package com.example.ludotoolz.ui.viewmodels

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ludotoolz.Datastore.UserDataStore
import com.example.ludotoolz.data.repositories.JeuPopulaireRepository
import com.example.ludotoolz.data.repositories.UtilisateurAvecJeuxRepository
import com.example.ludotoolz.data.repositories.UtilisateursRepository
import com.example.ludotoolz.database.models.JeuPopulaire
import com.example.ludotoolz.database.models.UtilisateurAvecJeux
import com.example.ludotoolz.network.JeuPopulaireService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class JeuxEcranViewModel(
    private val userDataStore: UserDataStore,
    private val jeuPopulaireService: JeuPopulaireService,
    private val jeuPopulaireRepository: JeuPopulaireRepository,
    private val utilisateurAvecJeuxRepository: UtilisateurAvecJeuxRepository,
    private val utilisateurRepository: UtilisateursRepository
) : ViewModel() {

    val jeuxPopulairesUiState: StateFlow<JeuxPopulaireUiState> =
        jeuPopulaireRepository.getAllJeuxPopulairesStream().map { JeuxPopulaireUiState(it) }
            .stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(TIMEOUT_MILLIS),
                initialValue = JeuxPopulaireUiState()
            )

    var utilisateurJeuxUiState by mutableStateOf(UtilisateurJeuxUiState())
        private set

    fun updateUtilisateurJeuxUiState() {
        viewModelScope.launch(Dispatchers.IO){
            utilisateurJeuxUiState = if(AsUtilisateurSelectionne()) {
                UtilisateurJeuxUiState(
                    utilisateurAvecJeuxDetails = UtilisateurAvecJeuxDetails(
                        utilisateurAvecJeux = utilisateurAvecJeuxRepository.getUtilisateurAvecJeuxStream(
                            utilisateurRepository.getIdByName(userDataStore.userName().first())
                        ).first()
                    ),
                )
            } else{
                UtilisateurJeuxUiState()
            }
        }
    }

    fun rafraichirListeJeuxPopulaires() {
        viewModelScope.launch(Dispatchers.IO){
            val response = jeuPopulaireService.get()
            if (response.isSuccessful) {
                val items = response.body()
                if (items != null) {
                    val jeuxPopulairesMap: List<JeuPopulaire> = items.totalItem.map {
                        JeuPopulaire(it.rang, it.nom, it.thumbnail, it.anneePublication)
                    }
                    jeuPopulaireRepository.insertAllPopulaire(*jeuxPopulairesMap.toTypedArray())
                }
            }
        }
    }

    suspend fun AsUtilisateurSelectionne(): Boolean {
        val nomUtilisateur = userDataStore.userName().first()
        return nomUtilisateur.isNotEmpty()
    }

    companion object {
        private const val TIMEOUT_MILLIS = 5_000L
    }
}

data class JeuxPopulaireUiState(val jeuxPopulaires: List<JeuPopulaire> = listOf())

data class UtilisateurJeuxUiState(val utilisateurAvecJeuxDetails: UtilisateurAvecJeuxDetails = UtilisateurAvecJeuxDetails())

data class UtilisateurAvecJeuxDetails(
    val utilisateurAvecJeux: UtilisateurAvecJeux = UtilisateurAvecJeux(),
)