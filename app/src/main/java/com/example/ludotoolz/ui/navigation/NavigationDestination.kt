package com.example.ludotoolz.ui.navigation

interface NavigationDestination {
    val route: String
    val titleRes: Int
}