package com.example.ludotoolz.ui.navigation

import androidx.annotation.StringRes
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import com.example.ludotoolz.R
import com.example.ludotoolz.ui.ecran.EntreeUtilisateurDestination
import com.example.ludotoolz.ui.ecran.EntreeUtilisateurEcran
import com.example.ludotoolz.ui.ecran.JeuxRoute
import com.example.ludotoolz.ui.ecran.ListeUtilisateursDestination
import com.example.ludotoolz.ui.ecran.UtilisateursRoute

sealed class Ecran(val route: String, @StringRes val titre: Int) {
    object Accueil : Ecran("jeux_route", titre = R.string.nom_application)
    object Utilisateurs : Ecran("utilisateurs_route", titre = R.string.entree_utilisateurs_tire)
}

fun NavGraphBuilder.utilisateurGraph(navController: NavHostController) {
    navigation(startDestination = ListeUtilisateursDestination.route, route = Ecran.Utilisateurs.route) {
        composable(ListeUtilisateursDestination.route) {
            UtilisateursRoute(
                navigateToUtilisateurEntry = {
                    navController.navigate(
                        EntreeUtilisateurDestination.route
                    )
                }
            )
        }
        composable(EntreeUtilisateurDestination.route) {
            EntreeUtilisateurEcran(
                navigateBack = { navController.popBackStack() },
                onNavigateUp = { navController.navigateUp() }
            )
        }
    }
}

fun NavGraphBuilder.jeuxEcran() {
    composable(route = JEUX_ROUTE) {
        JeuxRoute()
    }
}

fun NavController.navigateToJeux(navOptions: NavOptions? = null) {
    this.navigate(JEUX_ROUTE, navOptions)
}

fun NavController.navigateToUtilisateurs(navOptions: NavOptions? = null) = navigate(
    UTILISATEURS_ROUTE,
    navOptions
)