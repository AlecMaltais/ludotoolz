package com.example.ludotoolz.ui.viewmodels

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.example.ludotoolz.Datastore.UserDataStore
import com.example.ludotoolz.data.repositories.JeuRepository
import com.example.ludotoolz.data.repositories.UtilisateurAvecJeuxRepository
import com.example.ludotoolz.database.models.Utilisateur
import com.example.ludotoolz.data.repositories.UtilisateursRepository
import com.example.ludotoolz.database.models.Jeu
import com.example.ludotoolz.database.models.UtilisateurJeuCrossRef
import com.example.ludotoolz.network.JeuService
import com.example.ludotoolz.network.UtilisateursService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class EntreUtilisateurViewModel(
    private val utilisateursRepository: UtilisateursRepository,
    private val utilisateursService: UtilisateursService,
    private val jeuService: JeuService,
    private val jeuRepository: JeuRepository,
    private val utilisateurAvecJeuxRepository: UtilisateurAvecJeuxRepository,
    private val userDataStore: UserDataStore
) : ViewModel() {
    var utilisateurUiState by mutableStateOf(UtilisateurUiState())
        private set

    fun updateUiState(utilisateurDetails: UtilisateurDetails) {
        utilisateurUiState =
            UtilisateurUiState(
                utilisateurDetails = utilisateurDetails,
                estEntreeValide = validerEntree(utilisateurDetails)
            )
    }

    /**
     * Demande à l'API de retourner l'utilisateur correspondant au paramètre de la requête.
     * Enregistre l'utilsiateur dans la base de donnée si le nom de l'utilisateur reçue par
     * la requête correspond à celui du paramètre.
     *
     * (L'API retourne un utilisateur vide si le nom n'existe pas
     * dans la base de donnée de BoardGameGeek.)
     */
    suspend fun enregistrerUtilisateur(): Boolean {
        var estEnregistrer = false
        if (validerEntree() && !utilisateursRepository.verifieUtilisateurExiste(utilisateurUiState.utilisateurDetails.nomUtilisateur)) {
            val reponse =
                utilisateursService.get(utilisateurUiState.utilisateurDetails.nomUtilisateur)

            if (reponse.code() == 200 && utilisateurUiState.utilisateurDetails.nomUtilisateur == reponse.body()?.nom) {
                utilisateursRepository.insertUtilisateur(utilisateurUiState.utilisateurDetails.toUtilisateur())
                userDataStore.saveUserName(utilisateurUiState.utilisateurDetails.nomUtilisateur)
                estEnregistrer = true
            }
        }
        return estEnregistrer
    }

    private fun validerEntree(uiState: UtilisateurDetails = utilisateurUiState.utilisateurDetails): Boolean {
        return with(uiState) {
            nomUtilisateur.isNotBlank()
        }
    }

    /**
     * Enregistre la collection de jeux d'un utilisateur dans la base de données.
     *
     * Cette fonction effectue une requête asynchrone au service de jeux pour obtenir la collection de jeux de l'utilisateur.
     * Les jeux sont ensuite insérés dans la base de données locale, ainsi que les références aux jeux de l'utilisateur.
     */
    suspend fun enregistrerJeuxCollection() {
        CoroutineScope(Dispatchers.IO).launch {
            var reponse = jeuService.getJeux(utilisateurUiState.utilisateurDetails.nomUtilisateur)

            while (reponse.isSuccessful && reponse.code() == 202) {
                delay(5000)
                reponse = jeuService.getJeux(utilisateurUiState.utilisateurDetails.nomUtilisateur)
            }

            if (reponse.isSuccessful && reponse.body()?.totale!! > 0) {
                val items = reponse.body()
                if (items != null) {
                    val jeuxMap: List<Jeu> = items.listeItem.map {
                        Jeu(
                            jeuId = it.objectId,
                            titre = it.nom,
                            thumbnail = it.thumbnail,
                            image = it.image,
                            description = it.subtype,
                            joueursMin = it.statistique.minJoueurs,
                            joueursMax = it.statistique.maxJoueurs,
                            tempsMoyen = it.statistique.tempsJeu
                        )
                    }
                    jeuRepository.insertAllJeux(*jeuxMap.toTypedArray())

                    val utilisateurId =
                        utilisateursRepository.getIdByName(utilisateurUiState.utilisateurDetails.nomUtilisateur)
                    for (jeu in jeuxMap) {
                        utilisateurAvecJeuxRepository.insertUtilisateurjeuCrossRef(
                            utilisateurJeuCrossRef = UtilisateurJeuCrossRef(
                                utilisateurId,
                                jeu.jeuId
                            )
                        )
                    }
                }
            }
        }
    }
}

data class UtilisateurUiState(
    val utilisateurDetails: UtilisateurDetails = UtilisateurDetails(),
    val estEntreeValide: Boolean = false
)

data class UtilisateurDetails(
    val id: Int = 0,
    val nomUtilisateur: String = "",
    val estSelectionne: Boolean = true,
)

fun UtilisateurDetails.toUtilisateur(): Utilisateur = Utilisateur(
    nomUtilisateur = nomUtilisateur,
    estSelectionne = estSelectionne,
)

fun Utilisateur.toUtilisateurUiState(estEntreeValide: Boolean = false): UtilisateurUiState =
    UtilisateurUiState(
        utilisateurDetails = this.toUtilisateurDetails(),
        estEntreeValide = estEntreeValide
    )

fun Utilisateur.toUtilisateurDetails(): UtilisateurDetails = UtilisateurDetails(
    id = utilisateurId,
    nomUtilisateur = nomUtilisateur,
    estSelectionne = estSelectionne,
)