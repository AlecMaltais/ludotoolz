package com.example.ludotoolz.ui.ecran

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonColors
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.ludotoolz.R
import com.example.ludotoolz.database.models.Utilisateur
import com.example.ludotoolz.ui.composante.LudoToolzTopAppBar
import com.example.ludotoolz.ui.navigation.NavigationDestination
import com.example.ludotoolz.ui.viewmodels.ListeUtilisateursViewModel
import com.example.ludotoolz.ui.viewmodels.UtilisateursUiState
import org.koin.androidx.compose.koinViewModel

object ListeUtilisateursDestination: NavigationDestination {
    override val route = "liste_utilisateurs"
    override val titleRes = R.string.entree_utilisateurs_tire
}

@Composable()
fun UtilisateursRoute(
    modifier: Modifier = Modifier,
    navigateToUtilisateurEntry: () -> Unit,
    viewModel: ListeUtilisateursViewModel = koinViewModel()
) {
    ListeUtilisateursEcran(
        navigateToUtilisateurEntry = navigateToUtilisateurEntry,
        modifier = modifier,
        viewModel = viewModel,
    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ListeUtilisateursEcran(
    navigateToUtilisateurEntry: () -> Unit,
    modifier: Modifier = Modifier,
    viewModel: ListeUtilisateursViewModel,
) {
    val utilisateursUiState: UtilisateursUiState by viewModel.utilisateursUiState.collectAsState()

    val nomUtilisateurSelectionner: String? by viewModel.nomUtilisateurSelectionner.collectAsState(null)

    val scrollBehavior = TopAppBarDefaults.enterAlwaysScrollBehavior()

    Scaffold(
        modifier = modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
        topBar = {
            LudoToolzTopAppBar(
                titreRes = ListeUtilisateursDestination.titleRes,
                peuNaviguerArriere = false,
                navigateUp = {},
                scrollBehavior = scrollBehavior,
                actionIcon = Icons.Filled.Settings,
                actionIconContentDescription = "",
                onActionClick = {}
            )
        },
        floatingActionButton = {
            FloatingActionButton(
                onClick = navigateToUtilisateurEntry,
                shape = MaterialTheme.shapes.medium,
                modifier = Modifier.padding(dimensionResource(R.dimen.padding_grand))
            ) {
                Icon(
                    imageVector = Icons.Default.Add,
                    contentDescription = stringResource(R.string.entree_utilisateurs_tire)
                )
            }
        },
    ) { innerPadding ->
        UtilisateursCorps(
            utilisateurListe = utilisateursUiState.utilisateurList,
            onUtilisateurClick = viewModel::selectionnerUtilisateur,
            onUtilisateurDeleteClick = viewModel::supprimerUtilisateur,
            onUtilisateurUpdateClick = viewModel::updateCollectionRemote,
            utilisateurSelectionne = nomUtilisateurSelectionner,
            modifier = modifier
                .padding(innerPadding)
                .fillMaxSize()
        )
    }
}

@Composable
private fun UtilisateursCorps(
    utilisateurListe: List<Utilisateur>,
    onUtilisateurClick: (Utilisateur) -> Unit,
    onUtilisateurDeleteClick: (Utilisateur) -> Unit,
    onUtilisateurUpdateClick: (Utilisateur) -> Unit,
    utilisateurSelectionne: String?,
    modifier: Modifier = Modifier
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
    ) {
        if (utilisateurListe.isEmpty()) {
            Text(
                text = stringResource(R.string.aucun_utilisateurs_description),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.titleLarge
            )
        } else {
            ListeUtilisateurs(
                utilisateurListe = utilisateurListe,
                utilisateurSelectionne = utilisateurSelectionne,
                onUtilisateurClick = onUtilisateurClick,
                onUtilisateurDeleteClick = onUtilisateurDeleteClick,
                onUtilisateurUpdateClick = onUtilisateurUpdateClick,
                modifier = Modifier.padding(horizontal = dimensionResource(R.dimen.padding_petit))
            )
        }
    }
}

@Composable
private fun ListeUtilisateurs(
    utilisateurListe: List<Utilisateur>,
    utilisateurSelectionne: String?,
    onUtilisateurClick: (Utilisateur) -> Unit,
    onUtilisateurDeleteClick: (Utilisateur) -> Unit,
    onUtilisateurUpdateClick: (Utilisateur) -> Unit,
    modifier: Modifier = Modifier
) {
    LazyColumn(
        modifier = modifier
    ) {
        items(items = utilisateurListe, key = { it.utilisateurId }) { utilisateur ->
            val estSelectionne = utilisateur.nomUtilisateur.equals(utilisateurSelectionne)

            ElementUtilisateur(
                utilisateur = utilisateur,
                onUtilisateurClick = onUtilisateurClick,
                onUtilisateurDeleteClick = onUtilisateurDeleteClick,
                onUtilisateurUpdateClick = onUtilisateurUpdateClick,
                isSelected = estSelectionne,
                modifier = Modifier
                    .padding(dimensionResource(R.dimen.padding_petit))
            )
        }
    }
}

@Composable
private fun ElementUtilisateur(
    utilisateur: Utilisateur,
    isSelected: Boolean,
    onUtilisateurClick: (Utilisateur) -> Unit,
    onUtilisateurUpdateClick: (Utilisateur) -> Unit,
    onUtilisateurDeleteClick: (Utilisateur) -> Unit,
    modifier: Modifier = Modifier
) {

    val cardColor = if (isSelected) {
        CardDefaults.cardColors(MaterialTheme.colorScheme.primaryContainer)
    } else {
        CardDefaults.cardColors(MaterialTheme.colorScheme.surfaceVariant)
    }

    val textColor = if (isSelected) {
        MaterialTheme.colorScheme.onPrimaryContainer
    } else {
        MaterialTheme.colorScheme.onSurfaceVariant
    }

    val iconColors = if(isSelected) {
        IconButtonColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer,
            contentColor = MaterialTheme.colorScheme.onPrimaryContainer,
            disabledContainerColor = MaterialTheme.colorScheme.inverseSurface,
            disabledContentColor = MaterialTheme.colorScheme.inverseOnSurface
        )
    } else {
        IconButtonColors(
            containerColor = MaterialTheme.colorScheme.surfaceVariant,
            contentColor = MaterialTheme.colorScheme.onSurfaceVariant,
            disabledContainerColor = MaterialTheme.colorScheme.inverseSurface,
            disabledContentColor = MaterialTheme.colorScheme.inverseOnSurface
        )
    }

    Card(
        modifier = modifier.clickable { onUtilisateurClick(utilisateur) },
        elevation = CardDefaults.cardElevation(defaultElevation = 2.dp),
        colors = cardColor
    ) {
        Column(
            modifier = Modifier.padding(dimensionResource(R.dimen.padding_grand)),
            verticalArrangement = Arrangement.spacedBy(dimensionResource(R.dimen.padding_minime))
        ) {
            if(isSelected)
                Text(
                    text = "selectionne",
                    color = textColor,
                    fontStyle = FontStyle.Italic,
                    modifier = Modifier,
                    textAlign = TextAlign.Start
                )
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = utilisateur.nomUtilisateur,
                    color = textColor,
                    style = MaterialTheme.typography.titleLarge,
                    textAlign = TextAlign.Center
                )
                Row {
                    IconButton(onClick = { onUtilisateurUpdateClick(utilisateur) }, colors = iconColors) {
                        Icon(imageVector = Icons.Filled.Refresh,
                            contentDescription = "Mise à jour collection"
                        )
                    }
                    IconButton(onClick = { onUtilisateurDeleteClick(utilisateur) }, colors = iconColors) {
                        Icon(imageVector = Icons.Filled.Delete,
                            contentDescription = "Supprimer"
                        )
                    }
                }
            }
        }
    }
}