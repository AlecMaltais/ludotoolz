package com.example.ludotoolz.ui

import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.navigation.NavDestination
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navOptions
import androidx.tracing.trace
import com.example.ludotoolz.ui.navigation.JEUX_ROUTE
import com.example.ludotoolz.ui.navigation.TopLevelDestination
import com.example.ludotoolz.ui.navigation.TopLevelDestination.JEUX
import com.example.ludotoolz.ui.navigation.TopLevelDestination.UTILISATEURS
import com.example.ludotoolz.ui.navigation.UTILISATEURS_ROUTE
import com.example.ludotoolz.ui.navigation.navigateToJeux
import com.example.ludotoolz.ui.navigation.navigateToUtilisateurs
import kotlinx.coroutines.CoroutineScope

@Composable
fun rememberLudoAppState(
    coroutineScope: CoroutineScope = rememberCoroutineScope(),
    navController: NavHostController = rememberNavController()
): LudoAppState {
    return remember(
        navController,
        coroutineScope
    ) {
        LudoAppState(
            navController,
            coroutineScope
        )
    }
}

@Stable
class LudoAppState(
    val navController: NavHostController,
    val coroutineScope: CoroutineScope,
) {
    val destinationActuelle: NavDestination?
        @Composable get() = navController
            .currentBackStackEntryAsState().value?.destination

    val topLevelDestinationActuelle: TopLevelDestination?
        @Composable get() = when (destinationActuelle?.route) {
            JEUX_ROUTE -> JEUX
            UTILISATEURS_ROUTE -> UTILISATEURS
            else -> null
        }

    val topLevelDestinations: List<TopLevelDestination> = TopLevelDestination.entries

    fun navigateToTopLevelDestination(topLevelDestination: TopLevelDestination) {
        trace("Navigation: ${topLevelDestination.name}") {
            val topLevelNavOptions = navOptions {
                popUpTo(navController.graph.findStartDestination().id) {
                    saveState = true
                }
                launchSingleTop = true
                restoreState = true
            }

            when (topLevelDestination) {
                JEUX -> navController.navigateToJeux(topLevelNavOptions)
                UTILISATEURS -> navController.navigateToUtilisateurs(topLevelNavOptions)
            }
        }
    }
}