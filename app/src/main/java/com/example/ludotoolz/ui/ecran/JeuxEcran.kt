package com.example.ludotoolz.ui.ecran

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.paddingFromBaseline
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.example.ludotoolz.R
import com.example.ludotoolz.database.models.Jeu
import com.example.ludotoolz.database.models.JeuPopulaire
import com.example.ludotoolz.ui.viewmodels.JeuxEcranViewModel
import com.example.ludotoolz.ui.viewmodels.JeuxPopulaireUiState
import com.example.ludotoolz.ui.viewmodels.UtilisateurJeuxUiState
import org.koin.androidx.compose.koinViewModel

@Composable
fun JeuxRoute(
    modifier: Modifier = Modifier,
    viewModel: JeuxEcranViewModel = koinViewModel()
) {
    JeuxEcran(
        modifier = modifier,
        viewModel = viewModel,
    )
}

@Composable
fun JeuxEcran(
    modifier:Modifier = Modifier,
    viewModel: JeuxEcranViewModel = koinViewModel(),
) {
    val jeuxPopulaireUiState: JeuxPopulaireUiState by viewModel.jeuxPopulairesUiState.collectAsState()
    val utilisateurJeuxUiState: UtilisateurJeuxUiState = viewModel.utilisateurJeuxUiState
    var showCollection by remember { mutableStateOf(false) }
    var showDropdownMenu by remember { mutableStateOf(false) }

    Column {
        Row(modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp, vertical = 5.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            if(showCollection) {
                Text("Collection")
            } else {
                Text("Jeux Populaires")
            }
            Row {
                if(showCollection) {
                    IconButton(onClick = viewModel::updateUtilisateurJeuxUiState) {
                        Icon(
                            imageVector = Icons.Filled.Refresh,
                            contentDescription = "update"
                        )
                    }
                } else {
                    IconButton(onClick = viewModel::rafraichirListeJeuxPopulaires) {
                        Icon(
                            imageVector = Icons.Filled.Refresh,
                            contentDescription = "update"
                        )
                    }
                }
                IconButton(
                    onClick = { showDropdownMenu = true },
                ) {
                    Icon(
                        imageVector = Icons.Filled.MoreVert,
                        contentDescription = stringResource(id = R.string.dropdown)
                    )

                    DropdownMenu(
                        expanded = showDropdownMenu,
                        onDismissRequest = { showDropdownMenu = false }
                    ) {
                        DropdownMenuItem(
                            text = { Text(text = stringResource(id = R.string.collection)) },
                            leadingIcon = { Icon(
                                imageVector = Icons.Filled.PlayArrow,
                                contentDescription = stringResource(id = R.string.collection))},
                            onClick = {
                                viewModel.updateUtilisateurJeuxUiState()
                                showCollection = true
                            },
                        )
                        DropdownMenuItem(
                            text = { Text(stringResource(id = R.string.popular_games)) },
                            leadingIcon = {
                                Icon(
                                    imageVector = Icons.Filled.Favorite,
                                    contentDescription = stringResource(id = R.string.popular_games),
                                )
                            },
                            onClick = { showCollection = false }
                        )
                    }
                }
            }
        }
        if (showCollection) {
            JeuxCardList(
                jeux = utilisateurJeuxUiState.utilisateurAvecJeuxDetails.utilisateurAvecJeux.jeux,
                modifier = modifier,
            )
        } else {
            JeuxPopulairesCardList(
                jeuxPopulaires = jeuxPopulaireUiState.jeuxPopulaires,
                modifier = modifier,
            )
        }
    }
}

val jeuxCardModifier = Modifier
    .fillMaxWidth()
    .heightIn(100.dp, 150.dp)
    .padding(20.dp, 10.dp)


@Composable
private fun JeuxPopulairesCardList(
    jeuxPopulaires: List<JeuPopulaire>,
    modifier: Modifier = Modifier,
) {
    val jeuPopulaireScrollableState = rememberLazyListState()
    LazyColumn(
        modifier = modifier,
        verticalArrangement = Arrangement.spacedBy(20.dp),
        state = jeuPopulaireScrollableState
    ) {
        items(items = jeuxPopulaires, key = {it.rang}){ index ->
            JeuPopulaireCard(
                jeu = index,
                modifier = jeuxCardModifier
            )
        }
    }
}

@Composable
private fun JeuxCardList(
    jeux: List<Jeu>,
    modifier: Modifier = Modifier,
) {
    val jeuScrollableState = rememberLazyListState()
    if(jeux.isEmpty())
        Text(
            text = stringResource(id = R.string.collection_vide),
            fontWeight = FontWeight.Bold,
            fontStyle = FontStyle.Italic,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 50.dp, horizontal = 0.dp)
            )

    LazyColumn(
        modifier = modifier,
        verticalArrangement = Arrangement.spacedBy(20.dp),
        state = jeuScrollableState
    ) {
        items(items = jeux, key = {it.jeuId}){ index ->
            JeuCard(
                jeu = index,
                modifier = jeuxCardModifier
            )
        }
    }
}

@Composable
fun JeuPopulaireCard(
    jeu: JeuPopulaire,
    modifier: Modifier = Modifier
) {
    ElevatedCard(
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.surfaceVariant,
        ),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 6.dp
        ),
        modifier = modifier
    ) {
        Row {
            AsyncImage(
                model = jeu.thumbnail,
                contentDescription = "Une image",
                contentScale = ContentScale.FillBounds,
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f, true)
            )

            Column(modifier = Modifier.weight(2f)) {
                Row(
                    modifier = Modifier
                        .paddingFromBaseline(top = 20.dp)
                        .weight(2f)
                        .height(IntrinsicSize.Min)
                ) {
                    Text(
                        text = jeu.nom,
                        modifier = Modifier
                            .weight(2f)
                            .padding(start = 20.dp)
                            .wrapContentWidth(Alignment.Start),
                        textAlign = TextAlign.Start
                    )
                    Text(
                        text = jeu.anneePublication.toString(),
                        softWrap = false,
                        modifier = Modifier
                            .weight(1f)
                            .padding(end = 20.dp)
                            .wrapContentWidth(Alignment.End),
                        textAlign = TextAlign.End
                    )
                }
                Text(
                    text = jeu.rang.toString(),
                    modifier = Modifier
                        .weight(1f)
                        .padding(start = 20.dp)
                        .wrapContentWidth(Alignment.Start),
                    textAlign = TextAlign.Justify
                )
            }
        }
    }
}

@Composable
fun JeuCard(
    jeu: Jeu,
    modifier: Modifier = Modifier
) {
    ElevatedCard(
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.surfaceVariant,
        ),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 6.dp
        ),
        modifier = modifier
    ) {
        Row {
            AsyncImage(
                model = jeu.thumbnail,
                contentDescription = "Une image",
                contentScale = ContentScale.FillBounds,
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f, true)
            )

            Column(modifier = Modifier.weight(2f)) {
                Row(
                    modifier = Modifier
                        .paddingFromBaseline(top = 20.dp)
                        .weight(2f)
                        .height(IntrinsicSize.Min)
                ) {
                    Text(
                        text = jeu.titre,
                        modifier = Modifier
                            .weight(3f, true)
                            .padding(start = 20.dp)
                            .wrapContentWidth(Alignment.Start),
                        textAlign = TextAlign.Start
                    )
                    Text(
                        text = jeu.tempsMoyen.toString(),
                        modifier = Modifier
                            .weight(1f)
                            .padding(end = 20.dp)
                            .wrapContentWidth(Alignment.End),
                        textAlign = TextAlign.End
                    )
                }
                Text(
                    text = jeu.description,
                    modifier = Modifier
                        .weight(1f)
                        .padding(start = 20.dp)
                        .wrapContentWidth(Alignment.Start),
                    textAlign = TextAlign.Justify
                )
            }
        }
    }
}