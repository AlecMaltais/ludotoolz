package com.example.ludotoolz.ui.ecran

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewModelScope
import com.example.ludotoolz.R
import com.example.ludotoolz.ui.composante.LudoToolzTopAppBar
import com.example.ludotoolz.ui.navigation.NavigationDestination
import com.example.ludotoolz.ui.viewmodels.EntreUtilisateurViewModel
import com.example.ludotoolz.ui.viewmodels.UtilisateurDetails
import com.example.ludotoolz.ui.viewmodels.UtilisateurUiState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.compose.koinViewModel

object EntreeUtilisateurDestination: NavigationDestination {
    override val route = "entree_utilisateur"
    override val titleRes = R.string.entree_utilisateurs_tire
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EntreeUtilisateurEcran(
    navigateBack: () -> Unit,
    onNavigateUp: () -> Unit,
    canNavigateBack: Boolean = true,
    viewModel: EntreUtilisateurViewModel = koinViewModel()
) {
    val coroutineScope = rememberCoroutineScope()
    val snackbarHostState = remember { SnackbarHostState() }
    Scaffold(
        snackbarHost = {
            SnackbarHost(hostState = snackbarHostState)
        },
        topBar = {
            LudoToolzTopAppBar(
                titreRes = EntreeUtilisateurDestination.titleRes,
                peuNaviguerArriere = canNavigateBack,
                navigateUp = onNavigateUp,
                actionIcon = Icons.Filled.Settings,
                actionIconContentDescription = ""
            )
        }
    ) {innerPadding ->
        EntreeUtilisateurCorps(
            utilisateurUiState = viewModel.utilisateurUiState,
            onUtilisateurValueChanged = viewModel::updateUiState,
            onSaveClick = {
                coroutineScope.launch(Dispatchers.IO) {
                    if (viewModel.enregistrerUtilisateur()) {
                        viewModel.enregistrerJeuxCollection()
                        viewModel.viewModelScope.launch { navigateBack() }
                    } else {
                        snackbarHostState.showSnackbar(
                            message = "L'utilisateur n'existe pas",
                            duration = SnackbarDuration.Short)
                    }
                }
            },
            modifier = Modifier
                .padding(innerPadding)
                .verticalScroll(rememberScrollState())
                .fillMaxWidth()
        )
        
    }
}


@Composable
fun EntreeUtilisateurCorps(
    utilisateurUiState: UtilisateurUiState,
    onUtilisateurValueChanged: (UtilisateurDetails) -> Unit,
    onSaveClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier.padding(dimensionResource(R.dimen.padding_moyen)),
        verticalArrangement = Arrangement.spacedBy(dimensionResource(R.dimen.padding_grand))
    ) {
        UtilisateurInputForm(
            utilisateurDetails = utilisateurUiState.utilisateurDetails,
            onValueChange = onUtilisateurValueChanged,
            modifier = Modifier.fillMaxWidth(),
            enabled = utilisateurUiState.estEntreeValide
        )
        Button(
            onClick = onSaveClick,
            enabled = utilisateurUiState.estEntreeValide,
            shape = MaterialTheme.shapes.small,
            modifier = Modifier.fillMaxWidth().testTag("formButton")
        ) {
            Text(text = stringResource(R.string.action_enregistrer))
        }
    }
}

@Composable
fun UtilisateurInputForm(
    utilisateurDetails: UtilisateurDetails,
    modifier: Modifier = Modifier,
    onValueChange: (UtilisateurDetails) -> Unit = {},
    enabled: Boolean = true
) {
    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.spacedBy(dimensionResource(R.dimen.padding_moyen))
    ) {
        OutlinedTextField(
            value = utilisateurDetails.nomUtilisateur,
            onValueChange = {onValueChange(utilisateurDetails.copy(nomUtilisateur = it)) },
            label = { Text(stringResource(R.string.utilisateur_nom)) },
            colors = OutlinedTextFieldDefaults.colors(
                focusedContainerColor = MaterialTheme.colorScheme.secondaryContainer,
                unfocusedContainerColor = MaterialTheme.colorScheme.secondaryContainer,
                disabledContainerColor = MaterialTheme.colorScheme.secondaryContainer,
            ),
            modifier = Modifier.fillMaxWidth(),
            //enabled = enabled,
            singleLine = true
        )
        if (!enabled) {
            Text(
                text = stringResource(R.string.champs_requis),
                modifier = Modifier.padding(start = dimensionResource(R.dimen.padding_moyen))
            )
        }
    }
}