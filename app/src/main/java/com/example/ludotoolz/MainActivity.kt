package com.example.ludotoolz

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.example.ludotoolz.theme.LudoTheme
import com.example.ludotoolz.ui.LudoApp

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        installSplashScreen()

        setContent {
            LudoTheme(

            ) {
                LudoApp()
            }
        }
    }
}