package com.example.ludotoolz.data.repositories

import com.example.ludotoolz.database.models.UtilisateurAvecJeux
import com.example.ludotoolz.database.models.UtilisateurJeuCrossRef
import kotlinx.coroutines.flow.Flow

interface UtilisateurAvecJeuxRepository {

    fun getUtilisateurAvecJeuxStream(id: Int) : Flow<UtilisateurAvecJeux>

    suspend fun insertUtilisateurjeuCrossRef(utilisateurJeuCrossRef: UtilisateurJeuCrossRef)
}