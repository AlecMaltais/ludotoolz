package com.example.ludotoolz.data.repositories

import com.example.ludotoolz.database.models.Utilisateur
import kotlinx.coroutines.flow.Flow

interface UtilisateursRepository {

    fun getAllUtilisateursStream() : Flow<List<Utilisateur>>

    fun getUtilisateurStream(id: Int) : Flow<Utilisateur?>

    fun getUtilisateurByNameStream(username: String) : Flow<Utilisateur?>

    fun getIdByName(username: String) : Int

    fun verifieUtilisateurExiste(username: String) : Boolean

    suspend fun insertUtilisateur(utilisateur: Utilisateur)

    suspend fun deleteUtilisateur(utilisateur: Utilisateur)

    suspend fun updateUtilisateur(utilisateur: Utilisateur)
}