package com.example.ludotoolz.data.repositories

import com.example.ludotoolz.database.daos.UtilisateurJeuDao
import com.example.ludotoolz.database.models.UtilisateurAvecJeux
import com.example.ludotoolz.database.models.UtilisateurJeuCrossRef
import kotlinx.coroutines.flow.Flow

class OfflineUtilisateurAvecJeuxRepository(
    private val utilisateurJeuDao: UtilisateurJeuDao
) : UtilisateurAvecJeuxRepository {
    override fun getUtilisateurAvecJeuxStream(id: Int): Flow<UtilisateurAvecJeux> = utilisateurJeuDao.loadUtilisateurAvecJeuxById(id)

    override suspend fun insertUtilisateurjeuCrossRef(utilisateurJeuCrossRef: UtilisateurJeuCrossRef) = utilisateurJeuDao.insert(utilisateurJeuCrossRef)
}