package com.example.ludotoolz.data.repositories

import com.example.ludotoolz.data.model.ConfigDarkTheme
import com.example.ludotoolz.data.model.DonneeUtilisateur
import kotlinx.coroutines.flow.Flow

interface DonneUtilisateurRepository {

    val donneeUtilisateurStream: Flow<DonneeUtilisateur>

    suspend fun setConfigDarkTheme(configDarkTheme: ConfigDarkTheme)
}