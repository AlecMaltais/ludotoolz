package com.example.ludotoolz.data

object ConstantesAPI {
    const val PAGE_SIZE: Int = 20
    object BGGAPI {
        const val URL: String = "https://boardgamegeek.com/xmlapi/"
        const val NOM = "board_game_geek_api"
    }

    object BGGAPI2 {
        const val URL: String = "https://boardgamegeek.com/xmlapi2/"
        const val NOM = "board_game_geek_api_2"
    }
}