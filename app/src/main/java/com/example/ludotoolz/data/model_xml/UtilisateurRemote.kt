package com.example.ludotoolz.data.model_xml

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Root

@Root(name = "user", strict = false)
data class UtilisateurRemote @JvmOverloads constructor (

    @field:Attribute(name = "name")
    @param:Attribute(name = "name")
    var nom: String,
)