package com.example.ludotoolz.data.repositories

import com.example.ludotoolz.database.models.JeuPopulaire
import kotlinx.coroutines.flow.Flow

interface JeuPopulaireRepository {
    fun getAllJeuxPopulairesStream() : Flow<List<JeuPopulaire>>

    suspend fun insertAllPopulaire(vararg jeuxPopulaires: JeuPopulaire)

    suspend fun deleteJeuxPopulaires(vararg jeuxPopulaires: JeuPopulaire)

    suspend fun deleteAllPopulaire()
}