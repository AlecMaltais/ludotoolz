package com.example.ludotoolz.data.repositories

import com.example.ludotoolz.database.daos.UtilisateurDao
import com.example.ludotoolz.database.models.Utilisateur
import kotlinx.coroutines.flow.Flow

class OfflineUtilisateursRepository(
    private val utilisateurDao: UtilisateurDao
) : UtilisateursRepository {
    override fun getAllUtilisateursStream(): Flow<List<Utilisateur>> = utilisateurDao.loadUtilisateurs()

    override fun getUtilisateurStream(id: Int): Flow<Utilisateur?> = utilisateurDao.loadUtilisateurById(id)

    override fun getUtilisateurByNameStream(username: String): Flow<Utilisateur?> = utilisateurDao.loadUtilisateurByUserName(username)

    override fun getIdByName(username: String): Int = utilisateurDao.getIdByUserName(username)

    override fun verifieUtilisateurExiste(username: String): Boolean = utilisateurDao.verifieUtilisateurExiste(username)

    override suspend fun insertUtilisateur(utilisateur: Utilisateur) = utilisateurDao.insertUtilisateurs(utilisateur)

    override suspend fun deleteUtilisateur(utilisateur: Utilisateur) = utilisateurDao.deleteUtilisateur(utilisateur)

    override suspend fun updateUtilisateur(utilisateur: Utilisateur) = utilisateurDao.updateUtilisateurs(utilisateur)

}