package com.example.ludotoolz.data.model

enum class ConfigDarkTheme {
    SUIVRE_SYSTEME,
    LIGHT,
    DARK,
}