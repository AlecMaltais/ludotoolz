package com.example.ludotoolz.data.repositories

import com.example.ludotoolz.database.models.Jeu

interface JeuRepository {
    suspend fun insertAllJeux(vararg jeux: Jeu)

    suspend fun deleteJeu(jeu: Jeu)
}