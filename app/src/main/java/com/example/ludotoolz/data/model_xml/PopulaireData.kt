package com.example.ludotoolz.data.model_xml

import org.simpleframework.xml.Root
import org.simpleframework.xml.Attribute
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Path

@Root(name = "items", strict = false)
data class ItemsPopulaires @JvmOverloads constructor(

    @field:Attribute(name = "termsofuse")
    @param:Attribute(name = "termsofuse")
    var termsOfUse: String,

    @field:ElementList(name = "item", inline = true, required = false)
    @param:ElementList(name = "item", inline = true, required = false)
    var totalItem: List<ItemPopulaire>
)

@Root(name = "item", strict = false)
data class ItemPopulaire @JvmOverloads constructor(

    @field:Attribute(name = "id")
    @param:Attribute(name = "id")
    var id: Int,

    @field:Attribute(name = "rank")
    @param:Attribute(name = "rank")
    var rang: Int,

    @field:Attribute(name = "value")
    @param:Attribute(name = "value")
    @field:Path("thumbnail")
    @param:Path("thumbnail")
    var thumbnail: String,

    @field:Attribute(name = "value")
    @param:Attribute(name = "value")
    @field:Path("name")
    @param:Path("name")
    var nom: String,

    @field:Attribute(name = "value")
    @param:Attribute(name = "value")
    @field:Path("yearpublished")
    @param:Path("yearpublished")
    var anneePublication: Int
)