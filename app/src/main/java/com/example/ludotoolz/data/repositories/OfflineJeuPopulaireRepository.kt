package com.example.ludotoolz.data.repositories

import com.example.ludotoolz.database.daos.JeuPopulaireDao
import com.example.ludotoolz.database.models.JeuPopulaire
import kotlinx.coroutines.flow.Flow

class OfflineJeuPopulaireRepository(
    private val jeuPopulaireDao: JeuPopulaireDao
): JeuPopulaireRepository {
    override fun getAllJeuxPopulairesStream(): Flow<List<JeuPopulaire>> = jeuPopulaireDao
        .getALL()

    override suspend fun insertAllPopulaire(vararg jeuxPopulaires: JeuPopulaire) = jeuPopulaireDao
        .insertALL(*jeuxPopulaires)

    override suspend fun deleteJeuxPopulaires(vararg jeuxPopulaires: JeuPopulaire) = jeuPopulaireDao
        .delete(*jeuxPopulaires)

    override suspend fun deleteAllPopulaire() = jeuPopulaireDao.deleteALL()

}