package com.example.ludotoolz.data.repositories

import com.example.ludotoolz.database.daos.JeuDao
import com.example.ludotoolz.database.models.Jeu

class OfflineJeuRepository(
    private val jeuDao: JeuDao
): JeuRepository {
    override suspend fun insertAllJeux(vararg jeux: Jeu) = jeuDao.insertALL(*jeux)

    override suspend fun deleteJeu(jeu: Jeu) = jeuDao.delete(jeu)
}