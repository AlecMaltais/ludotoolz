package com.example.ludotoolz.data.model_xml

import org.simpleframework.xml.Root
import org.simpleframework.xml.Element
import org.simpleframework.xml.Attribute
import org.simpleframework.xml.ElementList

@Root(name = "items", strict = false)
data class Items @JvmOverloads constructor(

    @field:Attribute(name = "totalitems", empty = "0", required = false)
    @param:Attribute(name = "totalitems", empty = "0", required = false)
    var totale: Int = 0,

    @field:Attribute(name = "termsofuse", empty = "", required = false)
    @param:Attribute(name = "termsofuse", required = false)
    var termsOfUse: String = "",

    @field:ElementList(inline = true, empty = false, required = false)
    @param:ElementList(inline = true, empty = false,required = false)
    var listeItem: List<Item> = listOf()
)

@Root(name = "item", strict = false)
data class Item @JvmOverloads constructor(

    @field:Attribute(name = "subtype")
    @param:Attribute(name = "subtype")
    var subtype: String,

    @field:Attribute(name = "objectid")
    @param:Attribute(name = "objectid")
    var objectId: Int,

    @field:Element(name = "name")
    @param:Element(name = "name")
    var nom: String,

    @field:Element(name = "yearpublished")
    @param:Element(name = "yearpublished")
    var anneePublication: Int,

    @field:Element(name = "image")
    @param:Element(name = "image")
    var image: String,

    @field:Element(name = "thumbnail")
    @param:Element(name = "thumbnail")
    var thumbnail: String,

    @field:Element(name = "stats", required = false)
    @param:Element(name = "stats", required = false)
    var statistique: ItemStats = ItemStats(maxJoueurs = 0, maxTempsJeu = 0, minJoueurs = 0, minTempsJeu = 0, tempsJeu = 0)
)

@Root(name = "stats", strict = false)
data class ItemStats(

    @field:Attribute(name = "minplayers", required = false)
    @param:Attribute(name = "minplayers", required = false)
    var minJoueurs: Int,

    @field:Attribute(name = "maxplayers", required = false)
    @param:Attribute(name = "maxplayers", required = false)
    var maxJoueurs: Int,

    @field:Attribute(name = "minplaytime", required = false)
    @param:Attribute(name = "minplaytime", required = false)
    var minTempsJeu: Int,

    @field:Attribute(name = "maxplaytime", required = false)
    @param:Attribute(name = "maxplaytime", required = false)
    var maxTempsJeu: Int,

    @field:Attribute(name = "playingtime", required = false)
    @param:Attribute(name = "playingtime", required = false)
    var tempsJeu: Int,
)