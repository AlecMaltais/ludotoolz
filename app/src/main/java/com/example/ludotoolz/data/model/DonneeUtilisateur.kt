package com.example.ludotoolz.data.model

data class DonneeUtilisateur(
    val configDarkTheme: ConfigDarkTheme
)
