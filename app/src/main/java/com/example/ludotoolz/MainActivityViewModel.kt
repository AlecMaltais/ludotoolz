package com.example.ludotoolz

import androidx.lifecycle.ViewModel
import com.example.ludotoolz.data.model.DonneeUtilisateur
import com.example.ludotoolz.data.repositories.DonneUtilisateurRepository
import kotlinx.coroutines.flow.StateFlow

class MainActivityViewModel(
    donneUtilisateurRepository: DonneUtilisateurRepository
) : ViewModel() {
    //val uiState: StateFlow<MainActivityUiState> = donneUtilisateurRepository.
}

sealed interface MainActivityUiState {
    data object Loading : MainActivityUiState
    data class  Success(val donneeUtilisateur: DonneeUtilisateur) : MainActivityUiState
}