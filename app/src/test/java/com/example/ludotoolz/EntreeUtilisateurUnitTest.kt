package com.example.ludotoolz

import com.example.ludotoolz.Datastore.UserDataStore
import com.example.ludotoolz.data.repositories.OfflineJeuRepository
import com.example.ludotoolz.data.repositories.OfflineUtilisateurAvecJeuxRepository
import com.example.ludotoolz.data.repositories.OfflineUtilisateursRepository
import com.example.ludotoolz.database.daos.JeuDao
import com.example.ludotoolz.database.daos.UtilisateurDao
import com.example.ludotoolz.database.daos.UtilisateurJeuDao
import com.example.ludotoolz.database.models.Utilisateur
import com.example.ludotoolz.network.JeuService
import com.example.ludotoolz.network.UtilisateursService
import com.example.ludotoolz.ui.viewmodels.EntreUtilisateurViewModel
import com.example.ludotoolz.ui.viewmodels.UtilisateurDetails
import com.example.ludotoolz.ui.viewmodels.UtilisateurUiState
import com.example.ludotoolz.ui.viewmodels.toUtilisateur
import com.example.ludotoolz.ui.viewmodels.toUtilisateurDetails
import com.example.ludotoolz.ui.viewmodels.toUtilisateurUiState
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.test.assertEquals

class EntreeUtilisateurUnitTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainDispatcherRule = MainDispatcherRule(UnconfinedTestDispatcher())

    private val utilisateurDao = mockk<UtilisateurDao>()
    private val utilisateursService = mockk<UtilisateursService>()
    private val jeuService = mockk<JeuService>()
    private val jeuDao = mockk<JeuDao>()
    private val utilisateurJeuDao = mockk<UtilisateurJeuDao>()
    private val userDataStore = mockk<UserDataStore>()

    private lateinit var viewModel: EntreUtilisateurViewModel

    private val utilisateursDetailsValide = UtilisateurDetails(
        id = 6,
        nomUtilisateur = "testValide",
        estSelectionne = true
    )

    private val utilisateursDetailsInvalide = UtilisateurDetails(
        id = 0,
        nomUtilisateur = "testInvalide",
        estSelectionne = false
    )

    private val utilisateurValide = Utilisateur(
        utilisateurId = 6,
        nomUtilisateur = "userValide",
        estSelectionne = true
    )

    @Before
    fun SetUp (){
        viewModel = EntreUtilisateurViewModel(
            utilisateursRepository = OfflineUtilisateursRepository(utilisateurDao),
            utilisateursService,
            jeuService,
            jeuRepository = OfflineJeuRepository(jeuDao),
            utilisateurAvecJeuxRepository = OfflineUtilisateurAvecJeuxRepository(utilisateurJeuDao),
            userDataStore
        )
    }

    @Test
    fun `UtilisateurtoUtilisateurDetails retourne UtilisateurDetails`() = runTest{
        val nouvelUtilisateurDetails = utilisateurValide.toUtilisateurDetails()

        assertEquals(utilisateurValide.utilisateurId, nouvelUtilisateurDetails.id)
        assertEquals(utilisateurValide.nomUtilisateur, nouvelUtilisateurDetails.nomUtilisateur)
        assertEquals(utilisateurValide.estSelectionne, nouvelUtilisateurDetails.estSelectionne,)
    }

    @Test
    fun `UtilisateurToUtilisateurUiState retourne UtilisateurUiState`() = runTest {
        val nouvelUiState = utilisateurValide.toUtilisateurUiState(true)
        val utilisateurDetails = utilisateurValide.toUtilisateurDetails()

        assertEquals(utilisateurDetails, nouvelUiState.utilisateurDetails)
        assertEquals(true, nouvelUiState.estEntreeValide)
    }

    @Test
    fun `UtilsiateurDetailstoUtilisateur retourne Utilisateur`() = runTest{
        val nouvelUtilisateur = utilisateursDetailsInvalide.toUtilisateur()

        assertEquals(utilisateursDetailsInvalide.id, nouvelUtilisateur.utilisateurId)
        assertEquals(utilisateursDetailsInvalide.nomUtilisateur, nouvelUtilisateur.nomUtilisateur)
        assertEquals(utilisateursDetailsInvalide.estSelectionne, nouvelUtilisateur.estSelectionne,)
    }

    @Test
    fun `updateUiState valide`() = runTest{
        val uiStateInitial = viewModel.utilisateurUiState

        viewModel.updateUiState(utilisateursDetailsValide)

        assertEquals(UtilisateurUiState(), uiStateInitial)
        assertEquals(false, uiStateInitial.estEntreeValide)
        assertEquals(utilisateursDetailsValide, viewModel.utilisateurUiState.utilisateurDetails)
        assertEquals("testValide", viewModel.utilisateurUiState.utilisateurDetails.nomUtilisateur)
        assertEquals(true, viewModel.utilisateurUiState.estEntreeValide)
    }

    @Test
    fun `updateUiState invalide`() = runTest{
        val uiStateInitial = viewModel.utilisateurUiState

        viewModel.updateUiState(utilisateursDetailsInvalide)

        assertEquals(UtilisateurUiState(), uiStateInitial)
        assertEquals(false, uiStateInitial.estEntreeValide)
        assertEquals(utilisateursDetailsInvalide, viewModel.utilisateurUiState.utilisateurDetails)
        assertEquals("testInvalide", viewModel.utilisateurUiState.utilisateurDetails.nomUtilisateur)
        assertEquals(true, viewModel.utilisateurUiState.estEntreeValide)
    }

    @Test
    fun `valider entrer vide retourne faux`() = runTest {
        every { utilisateurDao.verifieUtilisateurExiste("") } returns false

        assertEquals(false, viewModel.enregistrerUtilisateur())
    }

    @Test
    fun `valider entrer valide retourne vraie`() = runTest {
        viewModel.updateUiState(utilisateursDetailsValide)

        assertEquals("testValide", viewModel.utilisateurUiState.utilisateurDetails.nomUtilisateur)
        assertEquals(true, viewModel.utilisateurUiState.estEntreeValide)
    }

    @Test
    fun `enregistrerUtilisateur existant retourne faux`() = runTest {
        every { utilisateurDao.verifieUtilisateurExiste("testInvalide") } returns true

        viewModel.updateUiState(utilisateursDetailsInvalide)

        assertEquals(true, viewModel.utilisateurUiState.utilisateurDetails.nomUtilisateur.isNotEmpty())
        assertEquals(false, viewModel.enregistrerUtilisateur())
    }
}