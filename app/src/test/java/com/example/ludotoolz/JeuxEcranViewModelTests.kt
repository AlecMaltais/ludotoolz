package com.example.ludotoolz

import com.example.ludotoolz.Datastore.UserDataStore
import com.example.ludotoolz.data.repositories.OfflineJeuPopulaireRepository
import com.example.ludotoolz.data.repositories.OfflineUtilisateurAvecJeuxRepository
import com.example.ludotoolz.data.repositories.OfflineUtilisateursRepository
import com.example.ludotoolz.database.daos.JeuPopulaireDao
import com.example.ludotoolz.database.daos.UtilisateurDao
import com.example.ludotoolz.database.daos.UtilisateurJeuDao
import com.example.ludotoolz.database.models.Jeu
import com.example.ludotoolz.database.models.JeuPopulaire
import com.example.ludotoolz.database.models.Utilisateur
import com.example.ludotoolz.database.models.UtilisateurAvecJeux
import com.example.ludotoolz.network.JeuPopulaireService
import com.example.ludotoolz.ui.viewmodels.JeuxEcranViewModel
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.test.assertEquals

class JeuxEcranViewModelTests {

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainDispatcherRule = MainDispatcherRule(UnconfinedTestDispatcher())

    private var jeuPopulaireService = mockk<JeuPopulaireService>(relaxed = true)
    private var userDataStore = mockk<UserDataStore>(relaxed = true)

    private var utilisateurDao = mockk<UtilisateurDao>(relaxed = true)
    private var jeuPopulaireDao = mockk<JeuPopulaireDao>(relaxed = true)
    private var utilisateurJeuDao = mockk<UtilisateurJeuDao>(relaxed = true)

    private lateinit var viewModel: JeuxEcranViewModel

    private val listePopulaire = listOf(
        JeuPopulaire(1, "Gloomhaven", "teststring", 2017),
        JeuPopulaire(2, "Everdell", "UnThumbnail", 2018)
    )

    private val listeJeuxPlein = listOf(
        Jeu(37, "Battletech", "BigStompyRobotThumbnail", "SameImage", "boardgame", 2, 4, 180),
        Jeu(96, "Rival Restaurants", "thumbnail", "image", "boardgame", 2, 6, 120),
        Jeu(12, "Battletech", "thumbnail", "image2", "boardgameexpansion", 2, 4, 180)
    )

    private val listeJeuxVide = listOf<Jeu>()

    private val utilisateurAvecJeux = Utilisateur(1, "ThunderBreaker", true)

    private val utilisateurSansJeux = Utilisateur(2, "Frank", false)

    @Before
    fun SetUp() {
        viewModel = JeuxEcranViewModel(
            userDataStore = userDataStore,
            jeuPopulaireService = jeuPopulaireService,
            jeuPopulaireRepository = OfflineJeuPopulaireRepository(jeuPopulaireDao),
            utilisateurAvecJeuxRepository = OfflineUtilisateurAvecJeuxRepository(utilisateurJeuDao),
            utilisateurRepository = OfflineUtilisateursRepository(utilisateurDao),
        )
        every { jeuPopulaireDao.getALL() } returns flowOf(listePopulaire)
        every { utilisateurJeuDao.loadUtilisateurAvecJeuxById(3) } returns flowOf(
            UtilisateurAvecJeux(
                utilisateurAvecJeux,
                listeJeuxPlein
            )
        )
        every { utilisateurJeuDao.loadUtilisateurAvecJeuxById(2) } returns flowOf(
            UtilisateurAvecJeux(
                utilisateurSansJeux,
                listeJeuxVide
            )
        )
    }

    @Test
    fun `AsUtilisateurSelectionne retourne vraie`() = runTest {
        every { jeuPopulaireDao.getALL() } returns flowOf(listePopulaire)
        every { userDataStore.userName() } returns flowOf("ThunderBreaker")

        assertEquals(true, viewModel.AsUtilisateurSelectionne())
    }

    @Test
    fun `AsUtilisateurSelectionne retourne faux`() = runTest {
        every { jeuPopulaireDao.getALL() } returns flowOf(listePopulaire)
        every { userDataStore.userName() } returns flowOf("")

        assertEquals(false, viewModel.AsUtilisateurSelectionne())
    }

    @Test
    fun `updateUtilisateurJeuxUiState sans utilisateur selectionner`() = runTest {
        every { jeuPopulaireDao.getALL() } returns flowOf(listePopulaire)
        every { userDataStore.userName() } returns flowOf("")

        val utilisateurJeuxUiStateInitial = viewModel.utilisateurJeuxUiState
        assertEquals(
            Utilisateur(0, "", true),
            utilisateurJeuxUiStateInitial.utilisateurAvecJeuxDetails.utilisateurAvecJeux.utilisateur
        )

        viewModel.updateUtilisateurJeuxUiState()
        assertEquals(utilisateurJeuxUiStateInitial, viewModel.utilisateurJeuxUiState)
    }
}