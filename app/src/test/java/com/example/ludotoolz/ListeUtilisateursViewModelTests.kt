package com.example.ludotoolz

import com.example.ludotoolz.Datastore.UserDataStore
import com.example.ludotoolz.data.repositories.OfflineJeuRepository
import com.example.ludotoolz.data.repositories.OfflineUtilisateurAvecJeuxRepository
import com.example.ludotoolz.data.repositories.OfflineUtilisateursRepository
import com.example.ludotoolz.database.daos.JeuDao
import com.example.ludotoolz.database.daos.UtilisateurDao
import com.example.ludotoolz.database.daos.UtilisateurJeuDao
import com.example.ludotoolz.database.models.Utilisateur
import com.example.ludotoolz.network.JeuService
import com.example.ludotoolz.ui.viewmodels.ListeUtilisateursViewModel
import io.mockk.coEvery
import io.mockk.coJustRun
import io.mockk.coVerify
import io.mockk.coVerifySequence
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.test.assertEquals

class ListeUtilisateursViewModelTests {

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainDispatcherRule = MainDispatcherRule(UnconfinedTestDispatcher())

    private var utilisateurDao = mockk<UtilisateurDao>(relaxed = true)
    private var jeuService = mockk<JeuService>(relaxed = true)
    private var jeuDao = mockk<JeuDao>(relaxed = true)
    private var utilisateurJeuDao = mockk<UtilisateurJeuDao>(relaxed = true)
    private var userDataStore = mockk<UserDataStore>(relaxed = true)

    private lateinit var viewModel: ListeUtilisateursViewModel

    private val utilisateurValide = Utilisateur(
        utilisateurId = 6,
        nomUtilisateur = "testValide",
        estSelectionne = false
    )

    private val listeUtilisateurs = listOf(
        utilisateurValide,
        Utilisateur(2, "Hank", true),
        Utilisateur(4, "BingoBango", false)
    )

    @Before
    fun SetUp() {
        viewModel = ListeUtilisateursViewModel(
            utilisateursRepository = OfflineUtilisateursRepository(utilisateurDao),
            jeuService,
            jeuRepository = OfflineJeuRepository(jeuDao),
            utilisateurAvecJeuxRepository = OfflineUtilisateurAvecJeuxRepository(utilisateurJeuDao),
            userDataStore
        )

        every { utilisateurDao.loadUtilisateurs() } returns flowOf(listeUtilisateurs)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `SelectionneUnUtilisateur change utilisateur selectionner`() = runTest {

        coJustRun { userDataStore.saveUserName("testValide") }
        coEvery { userDataStore.userName() } returns flowOf("testValide")

        assertEquals(null, viewModel.nomUtilisateurSelectionner.value)

        viewModel.selectionnerUtilisateur(utilisateurValide)

        coVerify(exactly = 1) { userDataStore.saveUserName("testValide") }
        verify { userDataStore.userName() }

        confirmVerified(userDataStore)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `SupprimerUtilisateur supprime l'utilisateur selectionner`() = runTest {
        coEvery { userDataStore.userName() } returns flowOf("testValide")
        coJustRun { userDataStore.saveUserName("") }
        coJustRun { utilisateurDao.deleteUtilisateur(utilisateurValide) }

        viewModel.supprimerUtilisateur(utilisateurValide)

        coVerifySequence {
            utilisateurDao.loadUtilisateurs()
            userDataStore.userName()
            userDataStore.userName()
            userDataStore.saveUserName("")
            utilisateurDao.deleteUtilisateur(utilisateurValide)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `SupprimerUtilisateur supprime l'utilisateur`() = runTest {
        coEvery { userDataStore.userName() } returns flowOf("Frank")
        coJustRun { userDataStore.saveUserName("") }
        coJustRun { utilisateurDao.deleteUtilisateur(utilisateurValide) }

        viewModel.supprimerUtilisateur(utilisateurValide)

        coVerifySequence {
            utilisateurDao.loadUtilisateurs()
            userDataStore.userName()
            userDataStore.userName()
            utilisateurDao.deleteUtilisateur(utilisateurValide)
        }

        coVerify(exactly = 0) { userDataStore.saveUserName("") }
    }
}